var autoprefixer = 	require('autoprefixer'),
	browserSync = 	require('browser-sync').create(),
	del = 			require('del'),
	mqpacker = 		require('css-mqpacker'),
	gulp = 			require('gulp'),
	concat = 		require('gulp-concat'),
	// cssnano = 		require('gulp-cssnano'),
	// htmlmin = 		require('gulp-htmlmin'),
	imagemin = 		require('gulp-imagemin'),
	postcss = 		require('gulp-postcss'),
	// rename = 		require('gulp-rename'),
	sass = 			require('gulp-sass'),
	// sourcemaps = 	require('gulp-sourcemaps'),
	// uglify = 		require('gulp-uglify'),
	path = 			require('path'),
	mergeRules = 	require('postcss-merge-rules'),
	reload = 		browserSync.reload;


gulp.task('html', function(){
	// var htmlminConfig = {
	// 	removeComments: true,
	// 	collapseWhitespace: true,
	// 	collapseBooleanAttributes: true,
	// 	removeAttributeQuotes: true,
	// 	removeRedundantAttributes: true,
	// 	removeEmptyAttributes: true,
	// 	removeScriptTypeAttributes: true,
	// 	removeStyleLinkTypeAttributes: true
	// };
	return gulp.src(['src/**/*.html'])
		// .pipe(htmlmin(htmlminConfig))
		.pipe(gulp.dest('dist'));
});

gulp.task('styles', function(){
	var sassConfig = {
		outputStyle: 'expanded',
		precision: 10
	};
	var AUTOPREFIXER_BROWSERS = [
		'ie >= 10',
		'ie_mob >= 10',
		'ff >= 30',
		'chrome >= 34',
		'safari >= 7',
		'opera >= 23',
		'ios >= 7',
		'android >= 4.4',
		'bb >= 10'
	];
	var postcssPlugins = [
		autoprefixer({ browsers: AUTOPREFIXER_BROWSERS }),
		mqpacker(),
		mergeRules()
	];
	return gulp.src(['src/styles/**/*.{scss,css}'])
		// .pipe(sourcemaps.init())
		.pipe(sass(sassConfig).on('error', sass.logError))
		.pipe(postcss(postcssPlugins))
		// .pipe(gulp.dest('dist/styles'))
		// .pipe(cssnano({ autoprefixer: false }))
		// .pipe(rename(function(path){ path.extname='.min.css'; }))
		// .pipe(sourcemaps.write())
		.pipe(gulp.dest('dist/styles'));
});

gulp.task('scripts', function(){
	var scriptsSource = [
		'src/scripts/jquery-3.2.1.min.js',
		'src/scripts/cssua.min.js',
		'src/scripts/main.js'
	];
	return gulp.src(scriptsSource)
		.pipe(concat('main.js'))
		// .pipe(gulp.dest('dist/scripts'))
		// .pipe(uglify())
		// .pipe(rename( function(path) { path.extname = '.min.js'; } ))
		.pipe(gulp.dest('dist/scripts'));
});

gulp.task('images', function(){
	var imageminConfig = {
		progressive: true,
		interlaced: true
	};
	return gulp.src(['src/images/**/*'])
		.pipe(imagemin(imageminConfig))
		.pipe(gulp.dest('dist/images'));
});

gulp.task('media', function(){
	return gulp.src(['src/media/**/*'])
		.pipe(gulp.dest('dist/media'));
});

gulp.task('icons', function(){
	return gulp.src(['src/icons/**/*'])
		.pipe(gulp.dest('dist/icons'));
});

gulp.task('serve', function(){
	browserSync.init({
		server: 'dist',
		port: 80,
		browser: 'chrome',
		open: false
	});

	gulp.watch(['src/**/*.html'], ['html', reload]);
	gulp.watch(['src/styles/**/*.{sass,scss,css}'], ['styles', reload]);
	gulp.watch(['src/scripts/**/*.js'], ['scripts', reload]);
	gulp.watch(['src/images/**/*.{jpg,jpeg,png,gif}'], ['images', reload]);
	gulp.watch(['src/media/**/*'], ['media', reload]);
	gulp.watch(['src/icons/**/*'], ['icons', reload]);

	gulp.watch('src/**/*').on('change', function (event) {
		if (event.type === 'deleted') {
			var filePathFromSrc = path.relative(path.resolve('src'), event.path),
				destFilePath = path.resolve('dist', filePathFromSrc);
			del.sync(destFilePath);
		}
	});
});

gulp.task('clean', function(){
	del(['dist/*', '!dist/.git']);
});

gulp.task('default', ['clean'], function(){
	gulp.start('html', 'styles', 'scripts', 'images', 'media', 'icons', 'serve');
});
