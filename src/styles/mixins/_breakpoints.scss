@import 'units';

// Define the minimum dimensions at which your layout will change,
// adapting to different screen sizes, for use in media queries.
$breakpoints: (
	xsmall: 20em,
	small: 30em,
	medium: 48em,
	large: 64em,
	xlarge: 78em,
	super: 116em
) !default;

// The length unit in which to output media query values.
// Supported values: px, em, rem.
$media-query-unit: em !default;

// Target media type: all, print, screen or speech
$default-media-type: all !default;

// Get a breakpoint's width
//
// @param {String} $name - Name of the breakpoint. One of $breakpoints
//
// @example scss
//  $tablet-width: breakpoint-width(tablet);
//  @media (min-width: breakpoint-width(desktop)) {}
//
// @requires {Variable} $breakpoints
//
// @returns {Number} Value in pixels
@function breakpoint-width($name, $unit: $media-query-unit) {
	@if map-has-key($breakpoints, $name) {
		$width: map-get($breakpoints, $name);
		@return convert-length($width, $unit);
	}
	@warn 'Breakpoint "#{$name}" wasn\'t found in "$breakpoints: #{$breakpoints}".';
}

// Name of the nth breakpoint, or null for the last breakpoint.
@function breakpoint-nth($n) {
	$names: map-keys($breakpoints);
	@if $n >= 1 and $n <= length($names) {
		@return nth($names, $n);
	}
	@warn 'Breakpoint number "#{$n}" wasn\'t found in "$breakpoints: #{$breakpoints}".';
}

// Name of the next breakpoint, or null for the last breakpoint.
@function breakpoint-next($name) {
	$names: map-keys($breakpoints);
	$n: index($names, $name);
	@if $n == null {
		@warn 'Breakpoint "#{$name}" wasn\'t found in "$breakpoints: #{$breakpoints}".';
	} @else if $n < length($names) {
		@return nth($names, $n + 1);
	} @else {
		@warn 'Breakpoint "#{$name}" is last in "$breakpoints: #{$breakpoints}". Returning null.';
		@return null;
	}
}

// Name of the prev breakpoint, or null for the last breakpoint.
@function breakpoint-prev($name) {
	$names: map-keys($breakpoints);
	$n: index($names, $name);
	@if $n == null {
		@warn 'Breakpoint "#{$name}" wasn\'t found in "$breakpoints: #{$breakpoints}".';
	} @else if $n > 1 {
		@return nth($names, $n - 1);
	} @else {
		@warn 'Breakpoint "#{$name}" is first in "$breakpoints: #{$breakpoints}". Returning null.';
		@return null;
	}
}

// Name of the first breakpoint.
@function breakpoint-first() {
	$names: map-keys($breakpoints);
	@if length($names) > 0 {
		@return nth($names, 1);
	} @else {
		@warn 'No breakpoints define in "$breakpoints: #{$breakpoints}". Returning null.';
		@return null;
	}
}

// Name of the last breakpoint.
@function breakpoint-last() {
	$names: map-keys($breakpoints);
	$length: length($names);
	@if $length != null and $length > 0 {
		@return nth($names, $length);
	} @else {
		@warn 'No breakpoints define in "$breakpoints: #{$breakpoints}". Returning null.';
		@return null;
	}
}

// Width of the nth breakpoint.
@function breakpoint-nth-width($n, $unit: $media-query-unit) {
	@return breakpoint-width(breakpoint-nth($n), $unit);
}

// Width of the next breakpoint.
@function breakpoint-next-width($name, $unit: $media-query-unit) {
	@return breakpoint-width(breakpoint-next($name), $unit);
}

// Width of the prev breakpoint.
@function breakpoint-prev-width($name, $unit: $media-query-unit) {
	@return breakpoint-width(breakpoint-prev($name), $unit);
}

// Width of the first breakpoint.
@function breakpoint-first-width($unit: $media-query-unit) {
	@return breakpoint-width(breakpoint-first(), $unit);
}

// Width of the last breakpoint.
@function breakpoint-last-width($unit: $media-query-unit) {
	@return breakpoint-width(breakpoint-last(), $unit);
}

// Media Query mixin
//
// @param {String | Boolean} $from (false) - One of $breakpoints
// @param {String | Boolean} $until (false) - One of $breakpoints
// @param {String | Boolean} $and (false) - Additional media query parameters
// @param {String} $media-type ($media-type) - Media type: screen, print…
//
// @ignore Undocumented API, for advanced use only:
// @ignore @param {Map} $breakpoints ($breakpoints)
// @ignore @param {String} $static-breakpoint ($static-breakpoint)
//
// @content styling rules, wrapped into a @media query when $responsive is true
//
// @requires {Variable} $media-type
// @requires {Variable} $breakpoints
// @requires {Variable} $static-breakpoint
// @requires {function} convert-length
// @requires {function} breakpoint-width
//
// @example scss
//  .element {
//    @include media-query($from: mobile) {
//      color: red;
//    }
//    @include media-query($until: tablet) {
//      color: blue;
//    }
//    @include media-query(mobile, tablet) {
//      color: green;
//    }
//    @include media-query($from: tablet, $and: '(orientation: landscape)') {
//      color: teal;
//    }
//    @include media-query(950px) {
//      color: hotpink;
//    }
//    @include media-query(tablet, $media-type: screen) {
//      color: hotpink;
//    }
//    // Advanced use:
//    $my-breakpoints: (L: 900px, XL: 1200px);
//    @include media-query(L:, $static-breakpoint: L) {
//      color: hotpink;
//    }
//  }
@mixin media-query($from: false, $until: false, $and: false, $media-type: $default-media-type, $unit: $media-query-unit) {

	$min-width: 0;
	$max-width: 0;
	$media-query: '';

	@if $from {
		@if type-of($from) == number {
			$min-width: convert-length($from, $unit);
		} @else {
			$min-width: convert-length(breakpoint-width($from), $unit);
		}
	}

	@if $until {
		@if type-of($until) == number {
			$max-width: convert-length($until, $unit);
		} @else {
			$o: if($unit == px, 0.01px, convert-length(0.01em, $unit));
			$max-width: convert-length(breakpoint-width($until), $unit) - $o;
		}
	}

	@if $min-width != 0	{
		$media-query: '#{$media-query} and (min-width: #{$min-width})';
	}
	@if $max-width != 0	{
		$media-query: '#{$media-query} and (max-width: #{$max-width})';
	}
	@if $and {
		$media-query: '#{$media-query} and #{$and}';
	}

	// Remove unnecessary media query prefix 'all and '
	@if ($media-type == all and $media-query != '') {
		$media-type: '';
		$media-query: str-slice(unquote($media-query), 6);
	}
	@media #{$media-type + $media-query} {
		@content;
	}
}