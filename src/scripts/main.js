(function($) {
	var WSK = {
		dev: function() {
			var body = $('body'),
				html = $('html');

			body.prepend('<div class="dev-bar"><span id="dev-grid-toggle" class="dev-option">Baseline</span></div>');
			$('#dev-grid-toggle').on('click', function() {
				body.css( 'height', function() { return $(document).height(); });
				if (body.is('.baseline--full')) {
					body.addClass('baseline--half').removeClass('baseline--full');
				} else if (body.is('.baseline--half')) {
					body.removeClass('baseline').removeClass('baseline--half');
				} else {
					body.addClass('baseline').addClass('baseline--full');
				}
			});

			$('.dev-bar').prepend('<span id="dev-theme-toggle" class="dev-option" href="#">Theme</span>');
			$('#dev-theme-toggle').on('click', function() {
				if (body.is('.theme-dark')) {
					body.removeClass('theme-dark');
				} else {
					body.addClass('theme-dark');
				}
			});

			$('.dev-bar').prepend('<span id="font-size" class="dev-option" href="#">'+ html.css('font-size') +'</span>');

			$(window).bind('resize orientationchange', function() {
				$('#font-size').replaceWith('<span id="font-size" class="dev-option" href="#">'+ html.css('font-size') +'</span>');
			});
		},
		responsive_media: function() {
			var container = $('.embed-responsive');
			var tags = container.find('audio, embed, iframe, img, object, video');

			tags.each(function() {

				var e = $(this);
				e.parent('.embed-responsive').addClass('embed-responsive--'+ e.prop('tagName').toLowerCase() );

				if ( !e.is('video') ) {
					var height = (e.is('[height]')) ? this.height : e.height();
					var width = (e.is('[width]')) ? this.width : e.width();
					var aspectRatio = (height/width)*100;
					var padding = (e.is('audio')) ? height+'px' : aspectRatio+'%';
					
					e.addClass('embed-responsive__item')
						.removeAttr('height')
						.removeAttr('width')
						.parent('.embed-responsive')
						.css('padding-bottom', padding);
				}
			});
		}
	};
	$(document).ready(function() {
		WSK.dev();
		WSK.responsive_media();
	});
	$(window).on('load', function() {});
	$(window).on('resize orientationchange', function() {});
})(jQuery);
