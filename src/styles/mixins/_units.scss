// @private Default font-size for all browsers
$browser-default-font-size: 16px;

// Convert any CSS <length> or <percentage> value to any another.
//
// @param `$length`
//   A css <length> or <percentage> value
//
// @param $to-unit
//   String matching a css unit keyword, e.g. 'em', '%', etc.
//
// @param $from-context
//   When converting from relative units, the absolute length (in px) to
//   which `$length` refers (e.g. for `$length`s in em units, would normally be the
//   font-size of the current element).
//
// @param $to-context
//   For converting to relative units, the absolute length in px to which the
//   output value will refer. Defaults to the same as $from-context, since it is
//   rarely needed.
@function convert-length($length, $to-unit, $from-context: $browser-default-font-size, $to-context: $from-context) {

	$from-unit: unit($length);

	// Optimize for cases where `from` and `to` units are accidentally the same.
	@if $from-unit == $to-unit {
		@return $length;
	}

	// Context values must be in px so we can determine a conversion ratio for
	// relative units.
	@if unit($from-context) != 'px' {
		@warn 'Paremeter "$from-context #{$from-context}" must resolve to a value in pixel units.';
	}
	@if unit($to-context) != 'px' {
		@warn 'Parameter "$to-context #{$to-context}" must resolve to a value in pixel units.';
	}

	// Convert input length to pixels
	$px-length: $length;

	@if $from-unit != 'px' {
		// Convert relative units using the from-context parameter.
		@if $from-unit == 'em'  {
			$px-length: $length * $from-context / 1em;
		} @else if $from-unit == 'rem' {
			$px-length: $length * $from-context / 1rem;
		} @else if $from-unit == '%'   {
			$px-length: $length * $from-context / 100%;
		} @else if $from-unit == 'ex'  {
			$px-length: $length * $from-context / 2ex;
		} @else if $from-unit == 'in' or $from-unit == 'mm' or $from-unit == 'cm' or $from-unit == 'pt' or $from-unit == 'pc' {
			// Convert absolute units using Sass' conversion table.
			$px-length: 0px + $length;
		} @else if $from-unit == 'ch' or $from-unit == 'vw' or $from-unit == 'vh' or $from-unit == 'vmin' {
			// Certain units can't be converted.
			@warn '#{$from-unit} units can\'t be reliably converted. Returning original value.';
			@return $length;
		} @else {
			@warn '#{$from-unit} is an unknown length unit. Returning original value.';
			@return $length;
		}
	}

	// Convert length in pixels to the output unit
	$output-length: $px-length;

	@if $to-unit != 'px' {
		@if $to-unit == 'em' {
			$output-length: $px-length * 1em / $to-context;
		} @else if $to-unit == 'rem' {
			$output-length: $px-length * 1rem / $to-context;
		} @else if $to-unit == '%'  {
			$output-length: $px-length * 100% / $to-context;
		} @else if $to-unit == 'ex' {
			$output-length: $px-length * 2ex / $to-context;
		} @else if $to-unit == 'in' {
			$output-length: 0in + $px-length;
		} @else if $to-unit == 'mm' {
			$output-length: 0mm + $px-length;
		} @else if $to-unit == 'cm' {
			$output-length: 0cm + $px-length;
		} @else if $to-unit == 'pt' {
			$output-length: 0pt + $px-length;
		} @else if $to-unit == 'pc' {
			$output-length: 0pc + $px-length;
		} @else if $to-unit == 'ch' or $to-unit == 'vw' or $to-unit == 'vh' or $to-unit == 'vmin' {
			// Non-convertible units
			@warn '#{$to-unit} units can\'t be reliably converted. Returning original value.';
			@return $length;
		} @else {
			@warn '#{$to-unit} is an unknown length unit. Returning original value.';
			@return $length;
		}
	}

	@return $output-length;
}

// Remove the unit of a length
//
// @param {Number} `$length`
//   Number to remove unit from
//
// @return {Number}
//   Unitless number
@function strip-unit($number) {
	@if type-of($number) == 'number' {
		@if unitless($number) == false {
			@return $number / ($number * 0 + 1);
		}
	} @else {
		@warn '"$number: #{$number}" is #{type-of($number)} value and must resolve to a number value.';
	}
	@return $number;
}

// Add `$unit` to `$value`
//
// @param {Number} $value
//   Value to add unit to
//
// @param {String} $unit
//   String representation of the unit
//
// @return {Number}
//   `$value` expressed in `$unit`
@function add-unit($value, $unit) {
	$units: (
		'px': 1px,
		'cm': 1cm,
		'mm': 1mm,
		'%': 1%,
		'ch': 1ch,
		'pc': 1pc,
		'in': 1in,
		'em': 1em,
		'rem': 1rem,
		'pt': 1pt,
		'ex': 1ex,
		'vw': 1vw,
		'vh': 1vh,
		'vmin': 1vmin,
		'vmax': 1vmax
	);
	@if index(map-keys($units), $unit) {
		@return $value * map-get($units, $unit);
	}
	@warn 'Invalid unit "#{$unit}".';
}

// String to number converter
// Casts a string into a number
//
// @param {String | Number} `$string`
//   Value to be parsed
//
// @return {Number}
@function to-number($string) {

	// $string: if(type-of($string) == string, unquote($string), $string);

	@if type-of($string) == number {
		@return $string;
	} @else if type-of($string) == string {

		$result: 0;
		$digits: 0;
		$minus: str-slice($string, 1, 1) == '-';
		$numbers: (
			'0': 0,
			'1': 1,
			'2': 2,
			'3': 3,
			'4': 4,
			'5': 5,
			'6': 6,
			'7': 7,
			'8': 8,
			'9': 9
		);

		@for $i from if($minus, 2, 1) through str-length($string) {

			$character: str-slice($string, $i, $i);

			@if not (index(map-keys($numbers), $character) or $character == '.') {
				@return add-unit(if($minus, -$result, $result), str-slice($string, $i));
			}

			@if $character == '.' {
				$digits: 1;
			} @else if $digits == 0 {
				$result: $result * 10 + map-get($numbers, $character);
			} @else {
				$digits: $digits * 10;
				$result: $result + map-get($numbers, $character) / $digits;
			}
		}

		@return if($minus, -$result, $result);

	} @else {
		@warn 'Value for "$string: #{$string}" should be a number or a string.';
		@return $string;
	}
}